// Package foodReport handles requests to the USDA Food Composition Database Food Reports API.
// This API is used to query the databases for in depth reports on individual food items within the database.
// A Food Reports database object must be referenced by its NDBNo, which is attainable by a call to the Search or Lists API first.
// API documentation available here: https://ndb.nal.usda.gov/ndb/doc/apilist/API-FOOD-REPORT.md
// Example URL with params: https://api.nal.usda.gov/ndb/reports/?ndbno=01009&type=f&format=json&api_key=DEMO_KEY
package foodReport

const (
	baseURL = "https://api.nal.usda.gov/ndb/V2/reports"
)
