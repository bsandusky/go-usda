# Food Reports

This package provides an interface to query the USDA Food Composition Databases Food Reports V2 endpoint. The endpoint provides full reporting on food item composition. Food items are retrievable with an NDBNo, which is available through the Search and Lists endpoints. This client library does not support the V1 api.

[Food Reports V2 database documentation](https://ndb.nal.usda.gov/ndb/doc/apilist/API-FOOD-REPORTV2.md)

## Request Parameters

APIKey & at least one NDBNo are required; others are optional. Up to 25 NDBNo values are possible.

The following defaults are in place:
- ~~ReportType default value is 'b' Basic. Other available options are 'f' Full or 's' Stats.~~ 
- The client library currently only supports Full reports since the data types for Basic and Stats types do not match data types in Full Report.
- Format default value is Go struct. Other available options are 'json' or 'xml'.

## Example

```
reportBuilder := foodReport.New()
req, _ := reportBuilder.APIKey("DEMO_KEY").NDBNo("01009").Build()
res, _ := req.Execute()
fmt.Println(res)
```