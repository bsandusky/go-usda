package foodReport

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
)

/****************
* REQUEST OBJECTS
*****************/

// Request interface implements Execute() method to call remote API.
type Request interface {
	Execute() (interface{}, error)
}

// request object is used to receive API call url with given params.
type request struct {
	url    *url.URL
	output string
}

/****************
* REQUEST METHODS
*****************/

func (r *request) Execute() (interface{}, error) {

	// create client
	client := http.Client{}
	req, _ := http.NewRequest("GET", r.url.String(), nil)

	// execute API call
	res, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer res.Body.Close()

	if r.output == "json" || r.output == "xml" {
		resp, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return nil, err
		}
		return resp, nil
	}

	// return marshalled json into Response object
	var resp Response
	json.NewDecoder(res.Body).Decode(&resp)
	return resp, nil
}
