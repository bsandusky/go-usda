package foodReport

import (
	"fmt"
	"net/url"
)

/**************************
* REQUEST BUILDER INTERFACE
***************************/

// RequestBuilder builds a request with given params in order to construct an API call.
// APIKey & at least one NDBNo are required; others are optional. Up to 25 NDBNo values are possible.
// The following defaults are in place:
// [DO NOT USE] ReportType default value is 'b' Basic. Other available options are 'f' Full or 's' Stats.
// The client library currently only supports Full reports since the data types for Basic and Stats types do
// not match data types in Full Report.
// Format default value is Go struct. Other available options are 'json' or 'xml'.
type RequestBuilder interface {
	APIKey(string) RequestBuilder
	NDBNo(string) RequestBuilder
	//ReportType(rune) RequestBuilder
	Format(string) RequestBuilder
	Build() (Request, error)
}

// requestBuilder object represents available parameters in order to filter results.
// This struct is used in order to implement the RequestBuilder interface.
type requestBuilder struct {
	apiKey string
	ndbno  string
	//reportType rune
	format string
}

/*******************************
* REQUEST BUILDER IMPLEMENTATION
********************************/

// New creates a new requestBuilder object to create a request
func New() RequestBuilder {
	return &requestBuilder{}
}

func (rb *requestBuilder) APIKey(key string) RequestBuilder {
	rb.apiKey = key
	return rb
}

func (rb *requestBuilder) NDBNo(id string) RequestBuilder {
	rb.ndbno = id
	return rb
}

// func (rb *requestBuilder) ReportType(reportType rune) RequestBuilder {
// 	rb.reportType = reportType
// 	return rb
// }

func (rb *requestBuilder) Format(format string) RequestBuilder {
	rb.format = format
	return rb
}

func (rb *requestBuilder) Build() (Request, error) {

	path, err := constructRequestURL(rb)
	if err != nil {
		return nil, err
	}

	return &request{
		url:    path,
		output: rb.format,
	}, nil
}

func constructRequestURL(rb *requestBuilder) (*url.URL, error) {

	out, err := url.Parse(baseURL)
	if err != nil {
		return nil, fmt.Errorf("error: URL parsing error")
	}

	query := out.Query()

	// set type 'f' for full since data types are different in other report types.
	query.Set("type", "f")

	if rb.apiKey != "" {
		query.Set("api_key", rb.apiKey)
	} else {
		return nil, fmt.Errorf("error: API key required")
	}

	if rb.ndbno != "" {
		query.Add("ndbno", rb.ndbno)
	} else {
		return nil, fmt.Errorf("error: at least one NDBNo is required")
	}

	// if rb.reportType != 0 {
	// 	if rb.reportType == 'b' || rb.reportType == 'f' || rb.reportType == 's' {
	// 		query.Set("type", fmt.Sprintf("%c", rb.reportType))
	// 	} else {
	// 		return nil, fmt.Errorf("error: sort value can only be b, f, or s")
	// 	}
	// }

	if rb.format != "" {
		if rb.format == "json" || rb.format == "xml" {
			query.Set("format", rb.format)
		} else {
			return nil, fmt.Errorf("error: format can only be json or xml")
		}
	}
	out.RawQuery = query.Encode()

	fmt.Println(out)
	return out, nil
}
