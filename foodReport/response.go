package foodReport

/*****************
* RESPONSE OBJECTS
******************/

// Response object is top level container for a Food objects.
type Response struct {
	Foods      []Foods `json:"foods" xml:"foods"`
	Count      uint    `json:"count" xml:"count"`
	NotFound   uint    `json:"notfound" xml:"notfound"`
	APIVersion float32 `json:"api" xml:"api"`
}

// Foods is a container object for each food item record.
type Foods struct {
	Food Food `json:"food" xml:"food"`
}

// Food object represents an individual list item returned in a response from the Food Reports API.
type Food struct {
	SRVersion   string      `json:"sr" xml:"sr"`
	Type        string      `json:"type" xml:"type"`
	Description Description `json:"desc" xml:"desc"`
	Ingredients Ingredients `json:"ing" xml:"ing"`
	Nutrients   []Nutrient  `json:"nutrients" xml:"nutrients"`
	Sources     []Source    `json:"sources" xml:"sources"`
	Footnotes   []Footnote  `json:"footnotes" xml:"footnotes"`
	Langual     []Langual   `json:"langual" xml:"langual"`
}

// Description object represents food item metadata fields.
type Description struct {
	NDBno              string `json:"ndbno" xml:"ndbno"`
	Name               string `json:"name" xml:"name'"`
	ShortDescription   string `json:"sd" xml:"sd"`
	FoodGroup          string `json:"fg" xml:"fg"`
	ScientificName     string `json:"sn" xml:"sn"`
	CommercialName     string `json:"cn" xml:"cn"`
	Manufacuturer      string `json:"manu" xml:"manu"`
	NToPFactor         uint   `json:"nf" xml:"nf"`
	CarbohydrateFactor uint   `json:"cf" xml:"cf"`
	FatFactor          uint   `json:"ff" xml:"ff"`
	ProteinFactor      uint   `json:"pf" xml:"pf"`
	RefusePercent      string `json:"r" xml:"r"`
	RefuseDescription  string `json:"rd" xml:"rd"`
	DatabaseSource     string `json:"ds" xml:"ds"`
	ReportingUnit      string `json:"ru" xml:"ru"`
}

// Ingredients object represents a list of ingredients for branded food products provided by the manufacturer.
type Ingredients struct {
	Description string `json:"desc" xml:"desc"`
	LastUpdate  string `json:"upd" xml:"upd"`
}

// Nutrient object contains nutrient data for each individual nutrient listed in a food item.
type Nutrient struct {
	NutrientID    uint      `json:"nutrient_id" xml:"nutrient_id"`
	Name          string    `json:"name" xml:"name"`
	Group         string    `json:"group" xml:"group"`
	Unit          string    `json:"unit" xml:"unit"`
	Derivation    string    `json:"derivation" xml:"derivation"`
	Value         float32   `json:"value" xml:"value"`
	SourceCode    string    `json:"sourcecode" xml:"sourcecode"`
	DataPoints    uint      `json:"dp" xml:"dp"`
	StandardError string    `json:"se" xml:"se"`
	Measures      []Measure `json:"measures" xml:"measures"`
}

// Measure object contains one set of values for nutrients in a food item.
type Measure struct {
	Label      string  `json:"label" xml:"label"`
	Equivalent float32 `json:"eqv" xml:"eqv"`
	Eunit      string  `json:"eunit" xml:"eunit"`
	Quantity   float32 `json:"qty" xml:"qty"`
	Value      float32 `json:"value" xml:"value"`
}

// Source object represents a reference source for food nutrient information in a Food Report.
type Source struct {
	ID        uint   `json:"id" xml:"id"`
	Title     string `json:"title" xml:"title"`
	Authors   string `json:"authors" xml:"authors"`
	Volume    string `json:"vol" xml:"vol"`
	Issue     string `json:"iss" xml:"iss"`
	Year      string `json:"year" xml:"year"`
	StartPage string `json:"start" xml:"start"`
	EndPage   string `json:"end" xml:"end"`
}

// Footnote object contains footnote text attached to a Food Report.
type Footnote struct {
	ID          uint   `json:"idv" xml:"idv"`
	Description string `json:"desc" xml:"desc"`
}

// Langual object represents LANGUAL codes assigned to the food item.
type Langual struct {
	ID          string `json:"id" xml:"id"`
	Description string `json:"desc" xml:"desc"`
}
