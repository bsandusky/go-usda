// Package list handles requests to the USDA Food Composition Database Lists API.
// This API is used to query the databases for lists of food, nutrients, and food groups.
// API documentation available here: https://ndb.nal.usda.gov/ndb/doc/apilist/API-LIST.md
// Example URL with params: https://api.nal.usda.gov/ndb/list?format=json&lt=f&sort=n&api_key=DEMO_KEY
package list

const (
	baseURL = "https://api.nal.usda.gov/ndb/list"
)
