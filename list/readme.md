# Lists

This package provides an interface to query the USDA Food Composition Databases Lists endpoint. This endpoint is retrieve Food and Nutrient identifiers for subsequent searches to Food and Nutrient Reports databases.

[Lists database documentation](https://ndb.nal.usda.gov/ndb/doc/apilist/API-SEARCH.md)

## Request Parameters

APIKey is required; all others are optional.

The following defaults are in place:
- ListType default value is 'f' Food. Other available options are 'd' Derivation code, 'n' All nutrients, 's' Specialty nutrients, 'r' Standard release nutrients only, 'g' Food group
- Max default value is '50'.
- Offset default value is '0'.
- Sort default value is 'r' (relevance). Other available option is 'n' (name).
- Format default value is Go struct. Other available options are 'json' or 'xml'.

## Example

```
listBuilder := list.New()
req, _ := listBuilder.APIKey("DEMO_KEY").Max(10).Build()
res, _ := req.Execute()
fmt.Println(res)
```