package list

import (
	"fmt"
	"net/url"
)

/**************************
* REQUEST BUILDER INTERFACE
***************************/

// RequestBuilder builds a request with given params in order to construct an API call.
// APIKey is required; all others are optional.
// The following defaults are in place:
// ListType default value is 'f' Food. Other available options are 'd' Derivation code, 'n' All nutrients), 's' Specialty nutrients, 'r' Standard release nutrients only, 'g' Food group
// Max default value is '50'
// Offset default value is '0'
// Sort default value is 'r' (relevance). Other available option is 'n' (name).
// Format default value is 'json'. Other available option is 'xml'.
type RequestBuilder interface {
	APIKey(string) RequestBuilder
	ListType(rune) RequestBuilder
	Max(uint) RequestBuilder
	Offset(uint) RequestBuilder
	Sort(rune) RequestBuilder
	Format(string) RequestBuilder
	Build() (Request, error)
}

// requestBuilder object represents available parameters in order to filter results.
// This struct is used in order to implement the RequestBuilder interface.
type requestBuilder struct {
	apiKey   string
	listType rune
	max      uint
	offset   uint
	sort     rune
	format   string
}

/*******************************
* REQUEST BUILDER IMPLEMENTATION
********************************/

// New creates a new requestBuilder object to create a request
func New() RequestBuilder {
	return &requestBuilder{}
}

func (rb *requestBuilder) APIKey(key string) RequestBuilder {
	rb.apiKey = key
	return rb
}

func (rb *requestBuilder) ListType(lt rune) RequestBuilder {
	rb.listType = lt
	return rb
}

func (rb *requestBuilder) Max(max uint) RequestBuilder {
	rb.max = max
	return rb
}

func (rb *requestBuilder) Offset(offset uint) RequestBuilder {
	rb.offset = offset
	return rb
}

func (rb *requestBuilder) Sort(sort rune) RequestBuilder {
	rb.sort = sort
	return rb
}

func (rb *requestBuilder) Format(format string) RequestBuilder {
	rb.format = format
	return rb
}

func (rb *requestBuilder) Build() (Request, error) {

	path, err := constructRequestURL(rb)
	if err != nil {
		return nil, err
	}

	return &request{
		url:    path,
		output: rb.format,
	}, nil
}

func constructRequestURL(rb *requestBuilder) (*url.URL, error) {

	out, err := url.Parse(baseURL)
	if err != nil {
		return nil, fmt.Errorf("error: URL parsing error")
	}

	query := out.Query()

	if rb.apiKey != "" {
		query.Set("api_key", rb.apiKey)
	} else {
		return nil, fmt.Errorf("error: API key required")
	}

	if rb.listType != 0 {
		if rb.listType == 'f' || rb.listType == 'd' || rb.listType == 'n' || rb.listType == 'g' {
			query.Set("lt", fmt.Sprintf("%c", rb.listType))
		} else if rb.listType == 's' || rb.listType == 'r' {
			query.Set("lt", fmt.Sprintf("n%c", rb.listType))

		} else {
			return nil, fmt.Errorf("error: list type value can only be f, d, n, s, r, or g")
		}
	}

	if rb.max != 0 {
		query.Set("max", fmt.Sprintf("%d", rb.max))
	}

	if rb.offset != 0 {
		query.Set("offset", fmt.Sprintf("%d", rb.offset))
	}

	if rb.sort != 0 {
		if rb.sort == 'n' || rb.sort == 'r' {
			query.Set("sort", fmt.Sprintf("%c", rb.sort))
		} else {
			return nil, fmt.Errorf("error: sort value can only be n or r")
		}
	}

	if rb.format != "" {
		if rb.format == "json" || rb.format == "xml" {
			query.Set("format", rb.format)
		} else {
			return nil, fmt.Errorf("error: format can only be json or xml")
		}
	}
	out.RawQuery = query.Encode()

	return out, nil
}
