package list

import (
	"reflect"
	"testing"
)

func TestExecute(t *testing.T) {
	rb := New()
	req, _ := rb.APIKey("DEMO_KEY").Build()

	res, err := req.Execute()
	if reflect.TypeOf(res) != reflect.TypeOf(Response{}) {
		t.Errorf("error: Execute() returns incorrect type. Got %v", reflect.TypeOf(res))
	}
	if err != nil {
		t.Errorf("Execute() error should be nil. Got %v", err)
	}

	rb = New()
	req, _ = rb.APIKey("DEMO_KEY").Format("json").Build()

	res, err = req.Execute()
	if reflect.TypeOf(res) != reflect.TypeOf([]byte{}) {
		t.Errorf("error: Execute() returns incorrect type. Got %v", reflect.TypeOf(res))
	}
	if err != nil {
		t.Errorf("Execute() error should be nil. Got %v", err)
	}

	rb = New()
	req, _ = rb.APIKey("DEMO_KEY").Format("xml").Build()

	res, err = req.Execute()
	if reflect.TypeOf(res) != reflect.TypeOf([]byte{}) {
		t.Errorf("error: Execute() returns incorrect type. Got %v", reflect.TypeOf(res))
	}
	if err != nil {
		t.Errorf("Execute() error should be nil. Got %v", err)
	}
}
