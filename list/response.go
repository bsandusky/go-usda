package list

/*****************
* RESPONSE OBJECTS
******************/

// Response object is top level container for nested List and Item objects
type Response struct {
	List List `json:"list" xml:"list"`
}

// List object represents the response object from the API.
type List struct {
	Type      string `json:"lt" xml:"lt"`
	Start     uint   `json:"start" xml:"start"`
	End       uint   `json:"end" xml:"end"`
	Total     uint   `json:"total" xml:"total"`
	Sort      rune   `json:"sort" xml:"sort"`
	SRVersion string `json:"sr" xml:"sr"`
	Items     []Item `json:"item" xml:"item"`
}

// Item object represents an individual list item returned in a response from the Lists API.
type Item struct {
	Offset uint   `json:"offset" xml:"offset"`
	ID     string `json:"id" xml:"id"`
	Name   string `json:"name" xml:"name'"`
}
