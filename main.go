package main

import (
	"fmt"

	"gitlab.com/bsandusky/go-usda/foodReport"
	"gitlab.com/bsandusky/go-usda/list"
	"gitlab.com/bsandusky/go-usda/nutrientReport"
	"gitlab.com/bsandusky/go-usda/search"
)

func main() {
	searchBuilder := search.New()
	req, _ := searchBuilder.APIKey("DEMO_KEY").Query("cheddar cheese").Max(10).Sort('r').Build()
	res, _ := req.Execute()
	fmt.Println(res)

	listBuilder := list.New()
	req, _ = listBuilder.APIKey("DEMO_KEY").Max(10).Sort('n').Build()
	res, _ = req.Execute()
	fmt.Println(res)

	reportBuilder := foodReport.New()
	req, _ = reportBuilder.APIKey("DEMO_KEY").NDBNo("01009").Build()
	res, _ = req.Execute()
	fmt.Println(res)

	nutrientBuilder := nutrientReport.New()
	req, _ = nutrientBuilder.APIKey("DEMO_KEY").NDBNo("01009").Nutrient(204).Nutrient(205).Build()
	res, _ = req.Execute()
	fmt.Println(res)
}
