// Package nutrientReport handles requests to the USDA Food Composition Database Nutrient Reports API.
// This API is used to query the databases for food reports with a subset of nutrient data returned from the database.
// A nutrient database object must be referenced by its nutrient_id, which is attainable by a call to the Lists API first.
// Up to 20 nutrient can be specified as query params for inclusion in the response.
// API documentation available here: https://ndb.nal.usda.gov/ndb/doc/apilist/API-NUTRIENT-REPORT.md
// Example URL with params: https://api.nal.usda.gov/ndb/nutrients/?format=json&api_key=DEMO_KEY&nutrients=205&nutrients=204&nutrients=208&nutrients=269&ndbno=01009
package nutrientReport

const (
	baseURL = "https://api.nal.usda.gov/ndb/nutrients"
)
