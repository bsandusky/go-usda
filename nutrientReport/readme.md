# Nutrients Report

This package provides an interface to query the USDA Food Composition Databases Nutrients Report endpoint. This endpoint is used to filter and limit the returned data to with up to 20 specific nutrients. Nutrients are identified as parameters with their ID values. These ID values are available through querying the Lists endpoint.

[Nutrient Reports database documentation](https://ndb.nal.usda.gov/ndb/doc/apilist/API-NUTRIENT-REPORT.md)

## Request Parameters

APIKey & at least one Nutrient value are required; others are optional.

The following defaults are in place:
- Food Group default value is "" for all food groups. Specify up to 10 groups as filters for search.
- Subset default value is 0. Other available option is 1. 0 returns a full report, with 1 returning an abridged report.
- Max default value is 50. A maximum of up to 1500 is possible.
- Offset default value is 0.
- Sort default value is 'f' Food Name. Other available option is 'c' Nutrient Content. If you are requesting more than one nutrient and specifying sort = c then the first nutrient in your list is used for the content sort.
- Format default value is Go struct. Other available options are 'json' or 'xml'.

## Example

```
nutrientBuilder := nutrientReport.New()
req, _ := nutrientBuilder.APIKey("DEMO_KEY").NDBNo("01009").Nutrient(204).Nutrient(205).Build()
res, _ := req.Execute()
fmt.Println(res)
```