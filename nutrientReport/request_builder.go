package nutrientReport

import (
	"fmt"
	"net/url"
)

/**************************
* REQUEST BUILDER INTERFACE
***************************/

// RequestBuilder builds a request with given params in order to construct an API call.
// APIKey & at least one Nutrient value are required; others are optional.
// The following defaults are in place:
// Food Group default value is "" for all food groups. Specify up to 10 groups as filters for search.
// Subset default value is 0. Other available option is 1. 0 returns a full report, with 1 returning an abridged report.
// Max default value is 50. A maximum of up to 1500 is possible.
// Offset default value is 0.
// Sort default value is 'f' Food Name. Other available option is 'c' Nutrient Content. If you are requesting more than one nutrient and specifying sort = c then the first nutrient in your list is used for the content sort.
// Format default value is Go struct. Other available options are 'json' or 'xml'.
type RequestBuilder interface {
	APIKey(string) RequestBuilder
	NDBNo(string) RequestBuilder
	FoodGroup(string) RequestBuilder
	Subset(uint) RequestBuilder
	Nutrient(uint) RequestBuilder
	Max(uint) RequestBuilder
	Offset(uint) RequestBuilder
	Sort(rune) RequestBuilder
	Format(string) RequestBuilder
	Build() (Request, error)
}

// requestBuilder object represents available parameters in order to filter results.
// This struct is used in order to implement the RequestBuilder interface.
type requestBuilder struct {
	apiKey    string
	ndbno     string
	foodGroup string
	subset    uint
	nutrients []uint
	max       uint
	offset    uint
	sort      rune
	format    string
}

/*******************************
* REQUEST BUILDER IMPLEMENTATION
********************************/

// New creates a new requestBuilder object to create a request
func New() RequestBuilder {
	return &requestBuilder{}
}

func (rb *requestBuilder) APIKey(key string) RequestBuilder {
	rb.apiKey = key
	return rb
}

func (rb *requestBuilder) NDBNo(id string) RequestBuilder {
	rb.ndbno = id
	return rb
}

func (rb *requestBuilder) FoodGroup(fg string) RequestBuilder {
	rb.foodGroup = fg
	return rb
}

func (rb *requestBuilder) Subset(subset uint) RequestBuilder {
	rb.subset = subset
	return rb
}

func (rb *requestBuilder) Nutrient(nutrient uint) RequestBuilder {
	rb.nutrients = append(rb.nutrients, nutrient)
	return rb
}

func (rb *requestBuilder) Max(max uint) RequestBuilder {
	rb.max = max
	return rb
}

func (rb *requestBuilder) Offset(offset uint) RequestBuilder {
	rb.offset = offset
	return rb
}

func (rb *requestBuilder) Sort(sort rune) RequestBuilder {
	rb.sort = sort
	return rb
}

func (rb *requestBuilder) Format(format string) RequestBuilder {
	rb.format = format
	return rb
}

func (rb *requestBuilder) Build() (Request, error) {

	path, err := constructRequestURL(rb)
	if err != nil {
		return nil, err
	}

	return &request{
		url:    path,
		output: rb.format,
	}, nil
}

func constructRequestURL(rb *requestBuilder) (*url.URL, error) {

	out, err := url.Parse(baseURL)
	if err != nil {
		return nil, fmt.Errorf("error: URL parsing error")
	}

	query := out.Query()

	if rb.apiKey != "" {
		query.Set("api_key", rb.apiKey)
	} else {
		return nil, fmt.Errorf("error: API key required")
	}

	if rb.ndbno != "" {
		query.Set("ndbno", rb.ndbno)
	}

	if rb.foodGroup != "" {
		query.Set("groups", rb.foodGroup)
	}

	if rb.subset == 1 {
		query.Set("subset", fmt.Sprintf("%d", rb.subset))
	}

	if len(rb.nutrients) > 0 {
		for _, n := range rb.nutrients {
			query.Add("nutrients", fmt.Sprintf("%d", n))
		}
	} else {
		return nil, fmt.Errorf("error: at least one nutrient_id is required")
	}

	if rb.max != 0 {
		query.Set("max", fmt.Sprintf("%d", rb.max))
	}

	if rb.offset != 0 {
		query.Set("offset", fmt.Sprintf("%d", rb.offset))
	}

	if rb.sort != 0 {
		if rb.sort == 'f' || rb.sort == 'c' {
			query.Set("sort", fmt.Sprintf("%c", rb.sort))
		} else {
			return nil, fmt.Errorf("error: sort value can only be f or c")
		}
	}

	if rb.format != "" {
		if rb.format == "json" || rb.format == "xml" {
			query.Set("format", rb.format)
		} else {
			return nil, fmt.Errorf("error: format can only be json or xml")
		}
	}
	out.RawQuery = query.Encode()

	return out, nil
}
