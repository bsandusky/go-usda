package nutrientReport

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	rb := &requestBuilder{}
	out := New()
	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: New() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestAPIKey(t *testing.T) {
	rb := New()
	out := rb.APIKey("DEMO_KEY")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: APIKey() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestNDBNo(t *testing.T) {
	rb := New()
	out := rb.NDBNo("01009")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: NDBNo() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestFoodGroup(t *testing.T) {
	rb := New()
	out := rb.FoodGroup("All groups")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: FoodGroup() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestSubset(t *testing.T) {
	rb := New()
	out := rb.Subset(1)

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Subset() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestMax(t *testing.T) {
	rb := New()
	out := rb.Max(10)

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Max() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestOffset(t *testing.T) {
	rb := New()
	out := rb.Offset(100)

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Offset() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestSort(t *testing.T) {
	rb := New()
	out := rb.Sort('r')

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Sort() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestFormat(t *testing.T) {
	rb := New()
	out := rb.Format("json")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Format() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestBuild(t *testing.T) {
	rb := New()
	out, err := rb.APIKey("DEMO_KEY").Nutrient(204).Build()

	if out != nil && reflect.TypeOf(out) != reflect.TypeOf(&request{}) {
		t.Errorf("error: Build() returned incorrect type. Got %v", err)
	}

	if err != nil {
		t.Errorf("error: Build() returned incorrect error. Got %v", err)
	}

	rb = New()
	out, err = rb.Format("json").Build()

	if out != nil {
		t.Errorf("error: Build() returned value without API key. Got %v", err)
	}

	if err != nil && err.Error() != "error: API key required" {
		t.Errorf("error: Build() returned incorrect error for missing API Key. Got %v", err)
	}
}

func TestConstructRequestURL(t *testing.T) {

	tests := []struct {
		in  requestBuilder
		qs  map[string]string
		err string
	}{
		// Successes
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			ndbno:     "01009",
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "ndbno": "01009"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			foodGroup: "All groups",
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "groups": "All groups"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			subset:    1,
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "subset": "1"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			max:       10,
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "max": "10"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			offset:    100,
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "offset": "100"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			sort:      'c',
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "sort": "c"},
			err: "",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			format:    "xml",
		}, qs: map[string]string{"api_key": "DEMO_KEY", "nutrients": "204", "format": "xml"},
			err: "",
		},

		// Errors
		{in: requestBuilder{},
			qs:  nil,
			err: "error: API key required",
		},
		{in: requestBuilder{
			apiKey: "DEMO_KEY",
		},
			qs:  nil,
			err: "error: at least one nutrient_id is required",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			sort:      'x',
		},
			qs:  nil,
			err: "error: sort value can only be f or c",
		},
		{in: requestBuilder{
			apiKey:    "DEMO_KEY",
			nutrients: []uint{204},
			ndbno:     "01009",
			format:    "soap",
		},
			qs:  nil,
			err: "error: format can only be json or xml",
		},
	}

	for _, tt := range tests {
		out, err := constructRequestURL(&tt.in)

		if err != nil && !reflect.DeepEqual(err.Error(), tt.err) {
			t.Errorf("incorrect error. Got %s, want %s", err.Error(), tt.err)
		}

		if out != nil {
			if len(out.Query()) != len(tt.qs) {
				t.Errorf("incorrect number of query values. Got %v, want %v", out.Query(), tt.qs)
			}

			for k, v := range tt.qs {
				if !reflect.DeepEqual(out.Query().Get(k), v) {
					t.Errorf("query values do not match. Got %s, want %s", out.Query().Get(k), v)
				}
			}
		}
	}
}
