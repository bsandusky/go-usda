package nutrientReport

// Response object is top level container for a Report object.
type Response struct {
	Report Report `json:"report" xml:"report"`
}

// Report object represents the response object from the Nutrients Reports API.
type Report struct {
	Group     string `json:"groups" xml:"groups"`
	Subset    string `json:"subset" xml:"subset"`
	SRVersion string `json:"sr" xml:"sr"`
	Start     uint   `json:"start" xml:"start"`
	End       uint   `json:"end" xml:"end"`
	Total     uint   `json:"total" xml:"total"`
	Food      []Food `json:"foods" xml:"foods"`
}

// Food object represents an individual list item returned in a response from the Nutrients Reports API.
type Food struct {
	NDBno     string     `json:"ndbno" xml:"ndbno"`
	Name      string     `json:"name" xml:"name'"`
	Weight    uint       `json:"weight" xml:"weight"`
	Measure   string     `json:"measure" xml:"measure"`
	Nutrients []Nutrient `json:"nutrients" xml:"nutrients"`
}

// Nutrient object contains nutrient data for each individual nutrient listed in a food item.
type Nutrient struct {
	NutrientID     string  `json:"nutrient_id" xml:"nutrient_id"`
	Nutrient       string  `json:"nutrient" xml:"nutrient"`
	Unit           string  `json:"unit" xml:"unit"`
	Value          string  `json:"value" xml:"value"`
	GramEquivalent float32 `json:"gm" xml:"gm"`
}
