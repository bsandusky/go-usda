# Go Client Library for USDA Food Composition Database

This set of Go packages provides a simple interface to accessing the USDA Food Composition Databases. These databases provide data on food items and their respective nutrient breakdowns. The interface to receiving this data is broken up into four packages, each corresponding to each database endpoint provided.

Food Reports must be retrieved by the Food NDBNo, which is available through a query to the Search or Lists endpoints.
Nutrient Reports must be retrieved using Nutrient ID parameters, which are avilable through a query to the Lists endpoint.

The default format of the API reponse is in native Go structs which correspond to the data returned. However, setting the Format paramater to either "json" or "xml" will return a `[]byte` in the indicated format. Each endpoint's interface in the client library follows a common builder pattern, allowing for setting of the various request parameters in a chained manner. Information about defaults as well as available options are in the readme.md file for each of the endpoints.

In order to use this API, you need to request a key. That can be done [here](https://ndb.nal.usda.gov/ndb/doc/index).

Examples ignore error handling, but you shouldn't.

[Link to USDA Food Composition Databases API documentation](https://ndb.nal.usda.gov/ndb/doc/index/)

## USDA Food Composition Databases Endpoint Documentation

1. [Search](https://ndb.nal.usda.gov/ndb/doc/apilist/API-SEARCH.md)
2. [Lists](https://ndb.nal.usda.gov/ndb/doc/apilist/API-SEARCH.md)
3. [Food Reports](https://ndb.nal.usda.gov/ndb/doc/apilist/API-FOOD-REPORTV2.md)
4. [Nutrient Reports](https://ndb.nal.usda.gov/ndb/doc/apilist/API-NUTRIENT-REPORT.md)

## Client Examples

Search
```
searchBuilder := search.New()
req, _ := searchBuilder.APIKey("DEMO_KEY").Query("cheddar cheese").Max(10).Sort('r').Build()
res, _ := req.Execute()
fmt.Println(res)
```

List
```
listBuilder := list.New()
req, _ := listBuilder.APIKey("DEMO_KEY").Max(10).Build()
res, _ := req.Execute()
fmt.Println(res)
```

Food Reports
```
reportBuilder := foodReport.New()
req, _ := reportBuilder.APIKey("DEMO_KEY").NDBNo("01009").Build()
res, _ := req.Execute()
fmt.Println(res)
```

Nutrient Reports
```
nutrientBuilder := nutrientReport.New()
req, _ := nutrientBuilder.APIKey("DEMO_KEY").NDBNo("01009").Nutrient(204).Nutrient(205).Build()
res, _ := req.Execute()
fmt.Println(res)
```
