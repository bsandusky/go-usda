# Search

This package provides an interface to query the USDA Food Composition Databases Search endpoint. This endpoint is primarily used to retrieve records in order to get an item's NDBNo for subsequent query to the Food Reports database.

[Search database documentation](https://ndb.nal.usda.gov/ndb/doc/apilist/API-SEARCH.md)


## Request Parameters

APIKey is required; all others are optional.

The following defaults are in place:
- DataSource default value is "" which returns all records. Other available options are "Branded Food Products" or "Standard Reference".
- FoodGroupID default value is "" which returns all records.
- Sort default value is 'r' (relevance). Other available option is 'n' (name).
- Max default value is '50'.
- Offset default value is '0'.
- Format default value is Go struct. Other available options are 'json' and 'xml'.

## Example

```
searchBuilder := search.New()
req, _ := searchBuilder.APIKey("DEMO_KEY").Query("cheddar cheese").Max(10).Sort('r').Build()
res, _ := req.Execute()
fmt.Println(res)
```