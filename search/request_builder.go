package search

import (
	"fmt"
	"net/url"
)

/**************************
* REQUEST BUILDER INTERFACE
***************************/

// RequestBuilder builds a request with given params in order to construct an API call.
// APIKey is required; all others are optional.
// The following defaults are in place:
// DataSource default value is "" which returns all records. Other available options are "Branded Food Products" or "Standard Reference".
// FoodGroupID default value is "" which returns all records.
// Sort default value is 'r' (relevance). Other available option is 'n' (name).
// Max default value is '50'.
// Offset default value is '0'.
// Format default value is Go struct. Other available options are 'json' and 'xml'.
type RequestBuilder interface {
	APIKey(string) RequestBuilder
	Query(string) RequestBuilder
	DataSource(string) RequestBuilder
	FoodGroup(string) RequestBuilder
	Sort(rune) RequestBuilder
	Max(uint) RequestBuilder
	Offset(uint) RequestBuilder
	Format(string) RequestBuilder
	Build() (Request, error)
}

// requestBuilder object represents available parameters in order to filter results.
// This struct is used in order to implement the RequestBuilder interface.
type requestBuilder struct {
	apiKey      string
	query       string
	dataSource  string
	foodGroupID string
	sort        rune
	max         uint
	offset      uint
	format      string
}

/*******************************
* REQUEST BUILDER IMPLEMENTATION
********************************/

// New creates a new requestBuilder object to create a request
func New() RequestBuilder {
	return &requestBuilder{}
}

func (rb *requestBuilder) APIKey(key string) RequestBuilder {
	rb.apiKey = key
	return rb
}

func (rb *requestBuilder) Query(query string) RequestBuilder {
	rb.query = query
	return rb
}

func (rb *requestBuilder) DataSource(ds string) RequestBuilder {
	rb.dataSource = ds
	return rb
}

func (rb *requestBuilder) FoodGroup(fg string) RequestBuilder {
	rb.foodGroupID = fg
	return rb
}

func (rb *requestBuilder) Sort(sort rune) RequestBuilder {
	rb.sort = sort
	return rb
}

func (rb *requestBuilder) Max(max uint) RequestBuilder {
	rb.max = max
	return rb
}

func (rb *requestBuilder) Offset(offset uint) RequestBuilder {
	rb.offset = offset
	return rb
}

func (rb *requestBuilder) Format(format string) RequestBuilder {
	rb.format = format
	return rb
}

func (rb *requestBuilder) Build() (Request, error) {

	path, err := constructRequestURL(rb)
	if err != nil {
		return nil, err
	}

	return &request{
		url:    path,
		output: rb.format,
	}, nil
}

// parse and construct request URL based on provided params
func constructRequestURL(rb *requestBuilder) (*url.URL, error) {

	out, err := url.Parse(baseURL)
	if err != nil {
		return nil, fmt.Errorf("error: URL parsing error")
	}

	query := out.Query()

	if rb.apiKey != "" {
		query.Set("api_key", rb.apiKey)
	} else {
		return nil, fmt.Errorf("error: API key required")
	}

	if rb.query != "" {
		query.Set("q", rb.query)
	}

	if rb.dataSource != "" {
		if rb.dataSource == "Branded Food Products" || rb.dataSource == "Standard Reference" {
			query.Set("ds", rb.dataSource)
		} else {
			return nil, fmt.Errorf("error: data source value can only be Branded Food Products or Standard Reference")
		}
	}

	if rb.foodGroupID != "" {
		query.Set("fg", rb.foodGroupID)
	}

	if rb.sort != 0 {
		if rb.sort == 'n' || rb.sort == 'r' {
			query.Set("sort", fmt.Sprintf("%c", rb.sort))
		} else {
			return nil, fmt.Errorf("error: sort value can only be n or r")
		}
	}

	if rb.max != 0 {
		query.Set("max", fmt.Sprintf("%d", rb.max))
	}

	if rb.offset != 0 {
		query.Set("offset", fmt.Sprintf("%d", rb.offset))
	}

	if rb.format != "" {
		if rb.format == "json" || rb.format == "xml" {
			query.Set("format", rb.format)
		} else {
			return nil, fmt.Errorf("error: format can only be json or xml")
		}
	}
	out.RawQuery = query.Encode()

	return out, nil
}
