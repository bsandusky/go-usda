package search

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	rb := &requestBuilder{}
	out := New()
	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: New() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestAPIKey(t *testing.T) {
	rb := New()
	out := rb.APIKey("DEMO_KEY")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: APIKey() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestQuery(t *testing.T) {
	rb := New()
	out := rb.Query("query")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Query() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestDataSource(t *testing.T) {
	rb := New()
	out := rb.DataSource("Standard Reference")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: DataSoure() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestFoodGroup(t *testing.T) {
	rb := New()
	out := rb.FoodGroup("Beef Products")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: FoodGroup() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestSort(t *testing.T) {
	rb := New()
	out := rb.Sort('r')

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Sort() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestMax(t *testing.T) {
	rb := New()
	out := rb.Max(10)

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Max() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestOffset(t *testing.T) {
	rb := New()
	out := rb.Offset(100)

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Offset() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestFormat(t *testing.T) {
	rb := New()
	out := rb.Format("json")

	if reflect.TypeOf(out) != reflect.TypeOf(rb) {
		t.Errorf("error: Format() returned incorrect type. Got %v", reflect.TypeOf(out))
	}
}

func TestBuild(t *testing.T) {
	rb := New()
	out, err := rb.APIKey("DEMO_KEY").Format("xml").Sort('r').Build()

	if out != nil && reflect.TypeOf(out) != reflect.TypeOf(&request{}) {
		t.Errorf("error: Build() returned incorrect type. Got %v", reflect.TypeOf(out))
	}

	if err != nil {
		t.Errorf("error: Build() returned incorrect error. Got %v", reflect.TypeOf(out))
	}

	rb = New()
	out, err = rb.Format("json").Build()

	if out != nil {
		t.Error("error: Build() returned value without API key")
	}

	if err != nil && err.Error() != "error: API key required" {
		t.Errorf("error: Build() returned incorrect error for missing API Key. Got %v", err)
	}
}

func TestConstructRequestURL(t *testing.T) {

	tests := []struct {
		in  requestBuilder
		qs  map[string]string
		err string
	}{
		// Successes

		{
			in: requestBuilder{
				apiKey: "DEMO_KEY",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey: "DEMO_KEY",
				query:  "butter",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey:     "DEMO_KEY",
				query:      "butter",
				dataSource: "Standard Reference",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter", "ds": "Standard Reference"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey:     "DEMO_KEY",
				query:      "butter",
				dataSource: "Standard Reference",
				sort:       'r',
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter", "ds": "Standard Reference", "sort": "r"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey:      "DEMO_KEY",
				query:       "butter",
				dataSource:  "Standard Reference",
				foodGroupID: "vegetables",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter", "ds": "Standard Reference", "fg": "vegetables"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey:     "DEMO_KEY",
				query:      "butter",
				dataSource: "Standard Reference",
				sort:       'r',
				max:        10,
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter", "ds": "Standard Reference", "sort": "r", "max": "10"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey:     "DEMO_KEY",
				query:      "butter",
				dataSource: "Standard Reference",
				sort:       'r',
				max:        10,
				offset:     50,
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter", "ds": "Standard Reference", "sort": "r", "max": "10", "offset": "50"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey:     "DEMO_KEY",
				query:      "butter",
				dataSource: "Standard Reference",
				sort:       'r',
				max:        10,
				offset:     50,
				format:     "xml",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "butter", "ds": "Standard Reference", "sort": "r", "max": "10", "offset": "50", "format": "xml"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey: "DEMO_KEY",
				query:  "cheddar cheese",
				sort:   'r',
				offset: 50,
				format: "xml",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "cheddar cheese", "sort": "r", "offset": "50", "format": "xml"},
			err: "",
		},
		{
			in: requestBuilder{
				apiKey: "DEMO_KEY",
				query:  "Cheddar Cheese",
				sort:   'r',
				offset: 50,
				format: "xml",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY", "q": "Cheddar Cheese", "sort": "r", "offset": "50", "format": "xml"},
			err: "",
		},

		// Errors
		{
			in:  requestBuilder{},
			qs:  nil,
			err: "error: API key required",
		},
		{
			in: requestBuilder{
				apiKey: "DEMO_KEY",
				query:  "Cheddar Cheese",
				sort:   'y',
				offset: 50,
				format: "xml",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY"},
			err: "error: sort value can only be 'n' or 'r'",
		},
		{
			in: requestBuilder{
				apiKey:     "DEMO_KEY",
				query:      "Cheddar Cheese",
				dataSource: "Wrong Source"},
			qs:  map[string]string{"api_key": "DEMO_KEY"},
			err: "error: data source value can only be 'Branded Food Products' or 'Standard Reference'",
		},
		{
			in: requestBuilder{
				apiKey: "DEMO_KEY",
				query:  "Cheddar Cheese",
				format: "soap",
			},
			qs:  map[string]string{"api_key": "DEMO_KEY"},
			err: "error: format can only be 'json' or 'xml'",
		},
	}

	for _, tt := range tests {
		out, err := constructRequestURL(&tt.in)

		if err != nil && !reflect.DeepEqual(err.Error(), tt.err) {
			t.Errorf("incorrect error. Got %s, want %s", err.Error(), tt.err)
		}

		if out != nil {
			if len(out.Query()) != len(tt.qs) {
				t.Errorf("incorrect number of query values. Got %v, want %v", out.Query(), tt.qs)
			}

			for k, v := range tt.qs {
				if !reflect.DeepEqual(out.Query().Get(k), v) {
					t.Errorf("query values do not match. Got %s, want %s", out.Query().Get(k), v)
				}
			}
		}
	}
}
