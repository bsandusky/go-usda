package search

/*****************
* RESPONSE OBJECTS
******************/

// Response object is top level container for nested List and Item objects
type Response struct {
	List List `json:"list" xml:"list"`
}

// List object represents the response object from the API. This includes the primary metadata
// for the response. It also includes individual Item objects.
type List struct {
	Query      string `json:"q" xml:"q"`
	SRVersion  string `json:"sr" xml:"sr"`
	DataSource string `json:"ds" xml:"ds"`
	Start      uint   `json:"start" xml:"start"`
	End        uint   `json:"end" xml:"end"`
	Total      uint   `json:"total" xml:"total"`
	FoodGroup  string `json:"group" xml:"group"`
	Sort       rune   `json:"sort" xml:"sort"`
	Items      []Item `json:"item" xml:"item"`
}

// Item object represents an individual list item returned in a response from the Search API.
// This object contains the NDBno, used in subsequent queries to the Food Reports & Nutrient Reports databases.
type Item struct {
	Offset        uint   `json:"offset" xml:"offset"`
	Group         string `json:"group" xml:"group"`
	Name          string `json:"name" xml:"name'"`
	NDBno         string `json:"ndbno" xml:"ndbno"`
	DataSource    string `json:"ds" xml:"ds"`
	Manufacuturer string `json:"manu" xml:"manu"`
}
