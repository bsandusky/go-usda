// Package search handles requests to the USDA Food Composition Database Search API.
// This API is used to query the databases for food items by string (Query param) for
// a list from which the NDBno can be obtained in order to query the Food Reports API.
// The NDBno is a unique identifier for a food item entry in the databases. It is required
// in order to query for a Food Report, which includes specific data on a food item's nutrients.
// API documentation available here: https://ndb.nal.usda.gov/ndb/doc/apilist/API-SEARCH.md
// Example URL with params: https://api.nal.usda.gov/ndb/search/?format=json&q=butter&sort=n&max=25&offset=0&api_key=DEMO_KEY
package search

const (
	// baseURL constant for Search API
	baseURL = "https://api.nal.usda.gov/ndb/search/"
)
